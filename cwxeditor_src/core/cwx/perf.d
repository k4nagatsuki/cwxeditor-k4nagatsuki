/// パフォーマンスカウンタ。コンソール/デバグビルドでない場合は無効。
module cwx.perf;

struct StackTrace {
	string file;
	size_t line;
}
StackTrace[] tStack, stStack;
size_t tStackLen = 0;
immutable S_TRACE = `.putStack(__FILE__, __LINE__);
	scope (exit) .tStackLen--;
	scope (failure) .saveStack();
`;
void putStack(string file, size_t line) {
	if (!.tStack.length) {
		.tStack = new StackTrace[8];
	} else if (.tStack.length <= .tStackLen) {
		.tStack.length *= 2;
	}
	.tStack[.tStackLen] = StackTrace(file, line);
	.tStackLen++;
}
void saveStack() {
	if (!stStack.length) {
		stStack = tStack[0 .. tStackLen].dup;
	}
}

debug {
	version (Console) {
		import std.datetime.stopwatch;
		import std.string;
		import std.conv;
		import std.stdio;

		std.datetime.stopwatch.StopWatch initTimer;
		static this () {
			initTimer.start();
		}

		__gshared ulong utperf = 0;
		__gshared ulong[1024u] t;
		shared static ~this () {
			foreach (i, time; t) {
				if (time > 0u) {
					writeln(format("%04d = ", i), time);
				}
			}
		}
		/// mixin(FPerf!N)でスコープ内の実行時間を計測する。
		template FPerf(int I) {
			static const FPerf
				= "import std.datetime.stopwatch;"
				~ "auto f_timer = std.datetime.stopwatch.StopWatch(std.datetime.stopwatch.AutoStart.yes);"
				~ "scope (exit) {"
				~ "f_timer.stop();"
				~ ".t[" ~ .to!string(I) ~ "] += f_timer.peek().total!\"msecs\";"
				~ "}";
		}
		/// mixin(BPerfS)とmixin(BPerf!N)でブロックの実行時間を計測する。
		const BPerfS
				= "static import std.datetime.stopwatch;"
				~ "auto b_timer = std.datetime.stopwatch.StopWatch(std.datetime.stopwatch.AutoStart.yes);";
		template BPerf(int I) {
			static const BPerf
				= "b_timer.stop();"
				~ ".t[" ~ .to!string(I) ~ "] += b_timer.peek().total!\"msecs\";"
				~ "b_timer.reset();"
				~ "b_timer.start();";
		}
		/// unittestの実行時間を計測する。
		static const UTPerf
			= "static import std.datetime.stopwatch;"
			~ "auto f_timer = std.datetime.stopwatch.StopWatch(std.datetime.stopwatch.AutoStart.yes);"
			~ "scope (exit) {"
			~ "f_timer.stop();"
			~ ".utperf += f_timer.peek().total!\"msecs\";"
			~ "}";
		static assert (FPerf!(10));
		static assert (BPerf!(10));
	} else {
		static immutable UTPerf = "";
	}
}
